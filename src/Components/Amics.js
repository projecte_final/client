import react from "react";
import { Container, Row, Col } from 'react-grid-system';
import Button from 'react-bootstrap/Button';
import '../Styles/Amics.css';
import Axios from "axios";
import Menu from './Menu';
import Header from './Header';
import ListGroup from 'react-bootstrap/ListGroup';
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'mdbreact/dist/css/mdb.css';

export default class Amics extends react.Component {

    constructor(props) {
        super(props);
        this.state = {
            numeroUsuaris: 0,
            showAfegir: false,
            show: true,
            showSolicituds: false,
            isDisabled: true,
            formularioCambiar: false,
            sendSolicitud: false,
            acceptSolicitud: false

        }
        this.mostrarLlistat = this.mostrarLlistat.bind(this);
        this.mostrarAfegir = this.mostrarAfegir.bind(this);
        this.mostrarSolicituds = this.mostrarSolicituds.bind(this);

    }
    async componentDidMount() {
        Axios.post("http://localhost:3001/api/mostrarSolicituds", {usuari2: JSON.parse(sessionStorage.getItem("Usuari")).usuari}).then((res) => this.setState({ Solicituds: res.data }) )
        Axios.post("http://localhost:3001/api/llistatAmics", {usuari2: JSON.parse(sessionStorage.getItem("Usuari")).usuari}).then((res) => {
             if(res.data.length > 0){
             this.setState({ llistatAmics: res.data })
        }
        } )
        const res = await Axios.get("http://localhost:3001/api/numeroUsuaris");
        this.setState({
            numeroUsuaris: res.data[0].usuari
        })
        const res1 = await Axios.get("http://localhost:3001/api/getUsuaris");
        var arrayUsers = [...res1.data];

        arrayUsers.splice(arrayUsers.findIndex((x) => x.usuari === JSON.parse(sessionStorage.getItem("Usuari")).usuari), 1);

        this.setState({
            usuaris: arrayUsers
        })
        arrayUsers = this.state.usuaris.map((x,i) =>{
            return({Usuari: x.usuari, clickEvent: (amic) => this.afegirAmic(amic)})
        })
        const datosTabla = {columns:[{label: 'Usuari', field: 'Usuari', sort: 'asc', width: 150}], rows:arrayUsers};
        this.setState({dataTable: datosTabla});
    }
    changeStat(x) {
        this.setState({ formularioCambiar: x });
    }
    mostrarLlistat() {
        this.setState({
            showAfegir: false,
            show: true,
            showSolicituds: false,
            sendSolicitud: false,
            acceptSolicitud: false
        })
    }
    mostrarAfegir() {
        this.setState({
            showAfegir: true,
            show: false,
            showSolicituds: false,
            sendSolicitud: true,
            acceptSolicitud: false
        })
    }
    mostrarSolicituds() {
        this.setState({
            show: false,
            showAfegir: false,
            showSolicituds: true,
            sendSolicitud: false,
            acceptSolicitud: true
        })
    }

    async enviarSolicitud(nomAmic) {
        var usuari2 = nomAmic;
        const res = await Axios.post("http://localhost:3001/api/enviarSolicitud", {usuari1: JSON.parse(sessionStorage.getItem("Usuari")).usuari, usuari2: usuari2}).then((res) => alert(res.data.mensaje));
    }

    async afegirAmic(amic) {
        var usuario = amic.Usuari;
        const res = await Axios.post("http://localhost:3001/api/GetMonstruos", {usuario: usuario})
        this.setState({
            formularioCambiar: true,
            monstresAmic: res.data.rows,
            nomAmic: amic.Usuari,
        })
    } 

    async mostrarPerfil(usuari1) {
        const res = await Axios.post("http://localhost:3001/api/GetMonstruos", {usuario: usuari1})
        this.setState({
            formularioCambiar: true,
            monstresAmic: res.data.rows,
            nomAmic: usuari1
        })
    }
    acceptarSolicitud(nomAmic) {
        var usuari1 = nomAmic;
        Axios.post("http://localhost:3001/api/acceptarSolicitud", {usuari1: usuari1 , usuari2: JSON.parse(sessionStorage.getItem("Usuari")).usuari}).then((res) => alert(res.data.mensaje));
        this.setState({ formularioCambiar: false });


    }

    render() {
        return (

            <div id="todo">

                <div className="clouds">

                </div>

                <div className="fogContainer">

                    <div className="fog">

                    </div>
                    {this.state.formularioCambiar ?
                        <div className="alignContent">
                            <div className="formulario">
                            </div>

                            <div className="background_Formularis formulariAmics">
                            <fieldset className="monstresDataGrid">
                                    <legend><b>{this.state.nomAmic}</b></legend>
                                    <table className="statsAmic">
                                        <tbody>
                                            <tr>
                                                <th>Monstre</th>
                                                <th>Nivell</th>
                                                <th>Dany</th>
                                                <th>Vida</th>
                                                <th>Armadura</th>
                                                <th>Esquiva</th>
                                                <th>Punts</th>
                                            </tr>
                                            {this.state.monstresAmic.map((x, i) => {
                                                return (
                                                    <tr key={i}>
                                                        {console.log("AQUIII", x)}
                                                        <td>{x.NomMonstre}</td>
                                                        <td>{Math.trunc(x.Punts_Gastats / 5)}</td>
                                                        <td>{x.Dany}</td>
                                                        <td>{x.Vida}</td>
                                                        <td>{x.Armadura}</td>
                                                        <td>{x.Esquiva}</td>
                                                        <td>{x.Punts_Actius}</td>
                                                    </tr>)
                                            })}
                                        </tbody>
                                    </table>
                                </fieldset>
                                <div className="formularioChangePass buttonsAmic">
                                    <div className="posicionButtonAcceptConfirmar">
                                        {this.state.sendSolicitud ? 
                                        <button className="button buttonAccept" onClick={() => this.enviarSolicitud(this.state.nomAmic)}>Enviar Solicitut</button>
                                    : null
                                    }
                                    { this.state.acceptSolicitud ?
                                        <button className="button buttonAccept" onClick={() => this.acceptarSolicitud(this.state.nomAmic)}>Acceptar Solicitut</button> 
                                        : null
                                    }
                                    
                                        
                                    </div>
                                    <div className="posicionButtonCancelConfirmar">
                                        <button className="button buttonCancel" onClick={() => this.changeStat(false)}>Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </div> : null}
                    <div id="containerlog" className="perfil" >
                        <Header userName={JSON.parse(sessionStorage.getItem("Usuari")).usuari} actiu={true} />
                        <Menu actiu={true}/>
                        <div className="background_Formularis amicsDiv">
                            
                            <br></br>
                            <div className="MenuAmics">
                                <button onClick={this.mostrarLlistat}>Llistat Amics</button>
                                <button onClick={this.mostrarAfegir}>Afegir Amics</button>
                                <button onClick={this.mostrarSolicituds}>Solicituds d'amistat</button>
                            </div>
                            
                            <br></br>
                            { this.state.showAfegir ? 
                                <div>
                                    {this.state.usuaris ? <div className="TaulaUsuaris" disabled={this.state.isDisabled}>
                                        <MDBDataTable 
                                            entries={5} 
                                            striped 
                                            hover 
                                            small 
                                            data={this.state.dataTable}
                                            displayEntries = {false}
                                            activeItem = {false}
                                            info = {false}
                                            />
                                    
                                    </div>: null}
                                </div>
                            : null }

                            { this.state.show ? 
                                <div>
                                   <table className="taulaSolicituds">
                                        <tbody>
                                            <tr>
                                                <th>
                                                    Amics
                                                </th>
                                            </tr>

                                            { this.state.llistatAmics ? this.state.llistatAmics.map((x, i) => {
                                                if (x.usuari1 == JSON.parse(sessionStorage.getItem("Usuari")).usuari){
                                                return (

                                                    <tr key={i} onClick={() => this.mostrarPerfil(x.usuari2)}> 
                                                        <td>{x.usuari2}</td>
                                                    </tr>
                                                    )
                                                } else{
                                                    return (
                                                        <tr key={i} onClick={() => this.mostrarPerfil(x.usuari1)}> 
                                                            <td>{x.usuari1}</td>
                                                        </tr>
                                                        )
                                                }
                                                }) : null}
                                        </tbody>
                                    </table>
                                    {!this.state.llistatAmics ? <p className="ColorAmigosTexto">Afagageix els teus amics a las seccio "Afegir Amics"</p>  : null}
                                </div>
                            : null }

                            { this.state.showSolicituds ? 
                                    <table className="taulaSolicituds">
                                        <tbody>
                                            <tr>
                                                <th>
                                                    Solicituds
                                                </th>
                                            </tr>

                                            { this.state.Solicituds ? this.state.Solicituds.map((x, i) => {
                                                return (
                                                    <tr key={i} onClick={() => this.mostrarPerfil(x.usuari1)}> 
                                                        <td>{x.usuari1} t'ha enviat una solicitut</td>
                                                    </tr>
                                                    )
                                                }) : null}
                                        </tbody>
                                    </table>
                            : null }

                        </div>
                    </div>
                </div>

            </div>


        );
    }
}
