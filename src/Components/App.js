import React from 'react';
import '../Styles/App.css';
import {
    Switch,
    Route,
    BrowserRouter,
    Redirect
} from "react-router-dom";
import Perfil from './Perfil';
import Login from './login';
import PrivateRoute from './PrivateRoute';
import Entrenamiento from './Entrenamiento';
import Settings from './Settings';
import Amics from './Amics';
import Pvp from './Pvp';
import administracion from './administracion';
import Expedicion from './Expedicion_Tablero';
function App() {
    
    return (
        <BrowserRouter>
            <div>
                <Switch>
                    <Route path="/login" component={ Login }/>
                    <PrivateRoute exact path="/Perfil" component={ Perfil }/>
                    <PrivateRoute exact path="/Entrenamiento" component={ Entrenamiento }/>
                    <PrivateRoute exact path="/Pvp" component={ Pvp }/>
                    <PrivateRoute exact path="/Amics" component={ Amics }/>
                    <PrivateRoute exact path="/Opcions" component={ Settings }/>
                    <PrivateRoute exact path="/Expedicion" component={ Expedicion }/>
                    <PrivateRoute exact path="/administracion" component={ administracion }/>
                    <Redirect to="/login"/>
                </Switch>
            </div>
        </BrowserRouter>
    );
}

export default App;