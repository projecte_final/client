import react from "react";
import { Container, Row, Col } from 'react-grid-system';
import Button from 'react-bootstrap/Button';
import '../Styles/expedicion.css';
import Axios from "axios";
import Menu from './Menu';
import Header from './Header';
import { ProgressBar } from 'react-bootstrap';
// import boton from '../imgs/Login/boton.png';
import { Link } from 'react-router-dom';
// import lluitarimg from '../imgs/Login/LLUITAR.png';
var boton ="https://bestiario.s-ul.eu/eJrajNQF";
var lluitarimg = "https://bestiario.s-ul.eu/knvDtF90";
// import 'bootstrap/dist/css/bootstrap.min.css';

export default class Expedicion_Tablero extends react.Component {
    constructor(props) {
        super(props);
        this.state = {
            actiu: false,
            isDisabled: true,
            show : true,
            vidaporturno_player: 0,
            vidaporturno_bot: 0,
            showstats: 0,
            Registro:[],
            ventanafinal: 0,
            resultado: "",
            bloqueado: "",
            esquivado: "",
            critico: "",
            Duelos:[],
            numeroduelo: 0,
            duelo_actual: 0,
            combatir: 0,
            showinv: 0

        };
        this.componentDidMount = this.componentDidMount.bind(this)  
  
    }
    async componentDidMount() {
        Axios.post("http://localhost:3001/api/GetImgMonstruos", { usuario: JSON.parse(sessionStorage.getItem("Usuari")).usuari }).then((res) => this.setState({
            imagenes: res.data.rows
        }))
        Axios.get("http://localhost:3001/api/tiposMonstruos").then((res) => this.setState({
        tipos: res.data.x
        }))
      
        const res = await Axios.get("http://localhost:3001/api/numeroMonstres"); 
        this.setState({
            numeroMonstres: res.data[0].monstres
        })
      
    }
   
    crear_arena(){
      var numeroarenas =  Math.trunc(Math.random() * (6 - 3) + 3);
        console.log(numeroarenas);
    for(var i = 0; i < numeroarenas; i++){
     if(i==0){
        var boton_ex = document.getElementById("boton_expedicion_jugar");
        var arena_aleatoria = document.getElementById("arena_aleatoria");
        const pelea_1 = document.createElement('button'); 
        var right = Math.trunc(Math.random() * (0 - 0));
        var bottom = Math.trunc(Math.random() * (0 - 100));
        console.log(right);
        pelea_1.type = 'button'; 
        pelea_1.innerText = 'Duelo '+i; 
        pelea_1.id = "p"+i;
        pelea_1.style.position = "relative";
        pelea_1.style.right =  right+"%";
        pelea_1.style.bottom =  bottom+"%";
        arena_aleatoria.appendChild(pelea_1); 
        }
       if(i==numeroarenas-1){
        var arena_aleatoria = document.getElementById("arena_aleatoria");
        const pelea_1 = document.createElement('button'); 
        pelea_1.type = 'button'; 
        pelea_1.innerText = 'BOSS-FIGHT'; 
        pelea_1.id = "Duelo"+i;
        pelea_1.style.position = "relative";
        pelea_1.style.right =  Math.trunc(Math.random() * (0 - 0))+"%";
        pelea_1.style.bottom =  Math.trunc(Math.random() * (0 - 100))+"%";
        arena_aleatoria.appendChild(pelea_1); 
        pelea_1.style.display = "none";
       }else{
        var arena_aleatoria = document.getElementById("arena_aleatoria");
        const pelea_1 = document.createElement('button'); 
        pelea_1.type = 'button'; 
        pelea_1.innerText = 'Duelo '+i; 
        pelea_1.id = "Duelo"+i;
        pelea_1.style.position = "relative";
        pelea_1.style.right =  Math.trunc(Math.random() * (0 - 0))+"%";
        pelea_1.style.bottom =  Math.trunc(Math.random() * (0 - 100))+"%"; 
        pelea_1.style.display = "none";
        arena_aleatoria.appendChild(pelea_1); 
       }
       
       this.state.Duelos.push(i)
       this.state.numeroduelo = this.state.numeroduelo +1;
     
    }
    this.setState({
        Duelos: this.state.Duelos,
        numeroduelo: this.state.numeroduelo
    })
    console.log(this.state.numeroduelo);
    }
     /*---------------------------------------------*/
     async  play(idmon,img_gif,danybot,vidabot,armadurabot,esquivabot) {
        this.setState({
            combatir: 1,
            showstats: 0,
            showinv: 1
        })
        const min = 1;
        console.log(" dany: "+danybot+" vida: "+vidabot+" arm: "+armadurabot+" esqu: "+esquivabot);
        var max = this.state.Monstruo[0].Dany + this.state.Monstruo[0].Armadura + this.state.Monstruo[0].Esquiva - 10;
        var playerAtackBase = this.state.Monstruo[0].Dany ;
        var playerDefensaBase = this.state.Monstruo[0].Armadura ;
        var playerEsquivaBase = this.state.Monstruo[0].Esquiva ;
        var playerVidaBase = this.state.Monstruo[0].Vida ;

        var atackBot = danybot;
        max = max - atackBot;
        var defensaBot = armadurabot;
        var esquivaBot = esquivabot;
        var vidaBot = vidabot;

        var arrayRandom = [atackBot, defensaBot, esquivaBot];
        var indexRandom = Math.trunc(Math.random() * (3 - 1));
   
        this.setState({
            vidaporturno_player: playerVidaBase,
            vidaporturno_bot: vidaBot
        })
        

        console.log("Suma Bot: " + (atackBot + defensaBot + esquivaBot) + " Max Jugador: " +  (this.state.Monstruo[0].Dany + this.state.Monstruo[0].Armadura + this.state.Monstruo[0].Esquiva));
        console.log("Valor Bot: " + atackBot + " " + defensaBot + " " + esquivaBot);

        

        
        this.getImgMonstre(img_gif);


        this.setState({
            show: false,
            randDMG : Math.round(atackBot),
            randARM : Math.round(defensaBot),
            randESQ : Math.round(esquivaBot),
            myText: 'My Original Text',
            monstre: 'monstre',
            monstreBot: 'monstreBot'
        })

        
        var ronda = 1;
        var torn = false;
        var resposta;
        var danyJugador;
        var danyBot;
      
        while (vidaBot >= 1 && playerVidaBase >= 1) {
           
    
          
              
            console.log("Ronda: " +ronda);
            this.state.Registro.push('RONDA: '+ronda)
           console.log(this.state.Registro);
            this.setState({
                monstre: '',
                monstreBot: '',
            })
            
            var defensaMajorP = false; 

            if(playerEsquivaBase < playerDefensaBase) {
                defensaMajorP = true;
            }

            var defensaMajorB = false; 

            if(esquivaBot < defensaBot) {
                defensaMajorB = true;
            }

            //********* TORN = TRUE LI TOCA AL JUGADOR ***********/

            
            if(ronda == 1){
                console.log("player agilidad: "+playerEsquivaBase+" bot esquiva: "+esquivaBot);
                if(playerEsquivaBase<esquivaBot){
                    console.log("PLAYER");
                    this.state.Registro.push('Jugador: ')
                    resposta = this.joc(defensaMajorP, playerAtackBase, playerDefensaBase,playerEsquivaBase, playerVidaBase);
                    danyJugador = resposta[0];
                    playerVidaBase = resposta[1];
                    ronda = ronda + 1;

                } else {
                    console.log("BOT");
                    this.state.Registro.push('BOT: '+ronda)
                    resposta = this.joc(defensaMajorB, atackBot, defensaBot,esquivaBot, vidaBot);
                    danyBot = resposta[0];
                    vidaBot = resposta[1];
                    torn = true;
                    ronda = ronda + 1;
                }
            } else if (torn) {
                console.log("PLAYER");
                resposta = this.joc(defensaMajorP, playerAtackBase, playerDefensaBase,playerEsquivaBase,playerVidaBase,danyBot);
                danyJugador = resposta[0];
                playerVidaBase = resposta[1];
                torn = false;
                ronda = ronda + 1;

            } else {
                console.log("BOT");
                resposta = this.joc(defensaMajorB, atackBot, defensaBot,esquivaBot,vidaBot,danyJugador);
                danyBot = resposta[0];
                vidaBot = resposta[1];
                torn = true;
                ronda = ronda + 1;
            }

            
            if (torn){
                this.setState({
                    monstre: 'monstre',
                    vidaporturno_player: playerVidaBase,
                    vidaporturno_bot: vidaBot,
                    
                })
                            await new Promise(resolve => setTimeout(resolve, 200));
                            this.setState({
                            monstreBot: 'invertir-color',
                           
                        })
                        console.log(this.state.monstreBot);
                        await new Promise(resolve => setTimeout(resolve, 200));
                            this.setState({
                            monstreBot: '',
                            critico:"",
                            bloqeuado:"",
                            esquivado:""
                        })
                
                 await new Promise(resolve => setTimeout(resolve, 2000));
           
            
            } else {
               
                this.setState({
                    monstreBot: 'monstreBot',
                    vidaporturno_player: playerVidaBase,
                    vidaporturno_bot: vidaBot,
                    
                    
                })
               await new Promise(resolve => setTimeout(resolve, 200));
                 this.setState({
                    monstre: 'invertir-color',                 
                })
                await new Promise(resolve => setTimeout(resolve, 200));
                 this.setState({
                    monstre: '',
                    critico:"",
                    bloqeuado:"",
                    esquivado:""
                })
                await new Promise(resolve => setTimeout(resolve, 2000));
              
            } 
        }
        if(vidaBot>0){
            
            this.setState({
                ventanafinal: 1,
                resultado: "win"
            })
           
               
        }else{
            this.setState({
                ventanafinal: 1,
                resultado: "losse"
            })
            this.nuevomonstruo(idmon);
        }
        
    }
    async getImgMonstre(monstreId){
        this.setState({
            monstreImgBot: monstreId
        })
      
    }
    nuevomonstruo(x){
        
         Axios.post("http://localhost:3001/api/nuevmonstruo", { usuario: JSON.parse(sessionStorage.getItem("Usuari")).usuari, id: x })
    }
    ganabot(){
        Axios.post("http://localhost:3001/api/sumarderrota", { usuario: JSON.parse(sessionStorage.getItem("Usuari")).usuari, idmonstruo: this.state.Monstruo[0].idMonstre })
                this.setState({
                    show: true,
                    Registro: [],
                    ventanafinal: 0,
                resultado: 0,
                combatir: 0,
                showstats: 0,
                showinv: 0
                })
                this.siguienteronda("perdido");
    }
    async ganajugador(){
        
            this.setState({
                show: true,
                Registro: [],
                ventanafinal: 0,
                resultado: 0,
                combatir: 0,
                showstats: 0,
                showinv: 0
            })
            await new Promise(resolve => setTimeout(resolve, 200));
                     window.location.reload(true);
            
           
           
    }
   
       joc(defensaMajor, atackBase, defensaBase, esquivaBase, vida, atackEnemic=0) {
        var dado100 = 20;
        const min = 1;
        var randAtk = (min + Math.random() * (dado100 - min));
        var esquiva = -1;
        var defensa = -1;
        var atack;

        //*********CALCULO ATAQUE************/
        var set=""
        if (randAtk >= 15) {
            this.setState({
                critico: "critico"
            })
     
           
            atack = atackBase * 2;
            atack = Math.round(atack + randAtk);
        } else if (randAtk <= 3) {
            atack = 0;
        } else  {
            atack = Math.round(atackBase + randAtk);
        }      
        

        var randomDeu = Math.random() * (10);
        
        

        if(randomDeu > 3){
            if(defensaMajor){
                defensa =  this.calculDefensa(defensaBase);
            } else {
                esquiva =  this.calculEsquiva(esquivaBase);
            }
            
        } else {
            if(!defensaMajor){
                defensa =  this.calculDefensa(defensaBase);
            } else {
                esquiva =  this.calculEsquiva(esquivaBase);
            }
        }

        
        if (defensa > -1 ) {
            if (atackEnemic > defensa){
            vida = vida - (atackEnemic - defensa);
            }else{
                this.setState({
                    bloqeuado: "bloq"
                })
            }
        } else {
            if ( atackEnemic > esquiva) {
                vida = vida - atackEnemic;
            } else{
                this.setState({
                    esquivado: "bloq"
                })
            }
        }
        this.setState({
            myText: vida,
        })
        console.log("Atack: " + atack);
        this.state.Registro.push('Ataque: '+atack)
        console.log("Esquiva: " + esquiva);
        this.state.Registro.push('Esquiva: '+esquiva)
        console.log("Defensa: " + defensa);
        this.state.Registro.push('Defensa: '+defensa)
        console.log("Vida: " + vida);
        return([atack, vida])

    }

    calculDefensa(defensaBase) {
        const min = 1;
        var defensa; 
        var dado100 = 20;
        var randDef = (min + Math.random() * (dado100 - min));
        //*********CALCULO DEFENSA************/
        if (randDef >= 15) {
            defensa = defensaBase * 2;
            defensa = Math.round(defensa + randDef);
        } else if (randDef <= 3) {
            defensa = 0;
        } else {
            defensa = Math.round(defensaBase + randDef);
        }
        return(defensa);
    }

     calculEsquiva(esquivaBase) {
        const min = 1;
        var dado120 = 30;
        var dado4 = 4;
        var esquiva = (min + Math.random() * (dado4 - min));

        //*********CALCULO ESQUIVA JUGADOR ************/

        if (esquiva == dado4){
            esquiva = 0;
        } else {
            esquiva = (min + Math.random() * (dado120 - min));
            if (esquiva >= 25){
                esquiva = 99999999;
            } else if (esquiva <= 5) {
                esquiva = 0;
            } else {
                esquiva = Math.round(esquiva + esquivaBase);
            }
        }
        return(esquiva);
    }
    
    /*---------------------------------------------*/
    siguienteronda(x){
        if(x=="ganado"){
            this.win();
        }
    }

    win(){
      
    }
    seleccionar_m(idmon,img_gif,danybot,vidabot,armadurabot,esquivabot){
        if(this.state.Monstruo == undefined){
            alert("Primer selecciona un monstre");
        }else{
        this.play(idmon,img_gif,danybot,vidabot,armadurabot,esquivabot);
    }
    }
    enseñar_stats(idmonstruo){
        Axios.post("http://localhost:3001/api/GetMonstruo_info", { usuario: JSON.parse(sessionStorage.getItem("Usuari")).usuari, idmonstruo: idmonstruo })
        .then((res)=> this.setState({
            Monstruo: res.data.rows ,
            nivel: Math.trunc(res.data.rows[0].Punts_Gastats / 5),
            showstats:1
         })  )
         console.log(this.state.showstats)
    }
    render() {
        return (
            <div id="todo">
             
             <div className="clouds"></div>
                <div className="fogContainer">
                    <div className="fog">  
                    </div>
            <div className="perfil" >
                <Header userName={JSON.parse(sessionStorage.getItem("Usuari")).usuari} actiu={true}/>
                <Menu actiu={true}/>
                {!this.state.showinv == 0 ? null:  <div id="inv_expedicion"> 
                <Row ><b id="Title_expedicion">Tria Un Monstre</b></Row>
               <Container id="cont_ent" className="cont_inv">
                            <Row id="row_ent">
                                     <Row>  {!this.state.imagenes ? "" : this.state.imagenes.map((x, i) => {
                                        return (
                                            <Row id="celda" sm={4} onClick={() => this.enseñar_stats(x.idMonstre)} key={i}> 
                                              
                                               
                                            <img width="10%" src={x.img} className="imatges imatges3" alt="imatgeMonstre"></img>
                                            <b id="text_ent_nom">{x.Nom}</b>
                                              
                                            </Row>
                                          
                                        )
                                    })}
                                   </Row>  
                               
                                 
                                </Row>
                                
                </Container>
                {this.state.showstats == 0 ? null :<div id="stats_exp">
                               
                               <div id="cuadro_Txt_exp"> 
                               <Row >
                                    ---------------
                                    </Row>
                               <Row id="txt_sta_exp">
                                       Nombre - <b ID="nom"> {this.state.Monstruo[0].Nombre_Perso == "" ||this.state.Monstruo[0].Nombre_Perso == null ? "Monstruo" : this.state.Monstruo[0].Nombre_Perso}</b>
                                    </Row>
                                    <Row id="txt_sta_exp">
                                     Nivel - <b id="nivel">{ Math.trunc(this.state.Monstruo[0].Punts_Gastats / 5)}</b>
                                    </Row>
                                    <Row >
                                    ---------------
                                    </Row>
                                    <Row id="txt_sta_exp">
                                        Vida - <b id="vida">{ Math.trunc(this.state.Monstruo[0].Vida)}</b>
                                    </Row>
                                    <Row id="txt_sta_exp">
                                        Daño - <b id="daño">{ Math.trunc(this.state.Monstruo[0].Dany)}</b>
                                    </Row>
                                    <Row id="txt_sta_exp">
                                        Armadura - <b id="armadura">{ Math.trunc(this.state.Monstruo[0].Armadura)}</b>
                                    </Row>
                                    <Row id="txt_sta_exp">
                                        Esquiva - <b id="esquiva">{ Math.trunc(this.state.Monstruo[0].Esquiva)}</b>
                                    </Row>
                                    <Row >
                                     
                                    </Row>
                                 </div>
                                              </div>  }
                       
                </div> } 
                <div id="tienda">
                <Row id="row_title_tienda"><b id="titulo_tienda">Aconsigueix un nou Monstre</b></Row>
                {!this.state.tipos ?  null : this.state.tipos.map((x, i) => {
                                        return (
                                            <Row id="filas">
                        <Col>
                        <img width="80%"src={lluitarimg} onClick={() => this.seleccionar_m(x.idMonstre,x.img_gif,x.Dany,x.Vida,x.Armadura,x.Esquiva)} alt=""/>
                        </Col>
                        <Col>
                           Nivell {x.nivel}
                        </Col>
                        <Col>
                            {this.state.tipos ?x.Nom: null}
                        </Col>
                        <Col>
                        {this.state.tipos ? <img width="60% " src={x.img} alt=""/>: null}
                        </Col>
                    </Row>
                                          
                                        )
                                    })}
                    
                   
                                  
                </div>
                {!this.state.combatir == 1 ? null:
                <div id="arenadecombate">
                          {!this.state.Monstruo ?  "": 
                            <div>
                                <progress id="barra_jugador" value={this.state.vidaporturno_player} max={this.state.Monstruo[0].Vida}/>
                               

                                <div className={this.state.monstre} id="monstresPlayer1">
                                   
                                    <Row> 
                                    <b ID="nom"> {this.state.Monstruo[0].Nombre_Perso == "" ||this.state.Monstruo[0].Nombre_Perso == null ? "Monstruo" : this.state.Monstruo[0].Nombre_Perso}</b>
                                        <img src={this.state.Monstruo[0].img_gif } className="imatges" alt="imatgeMonstre"></img>  
                                    
                                    </Row>
                                    {}
                                </div>
                               
                                {this.state.monstreImgBot ?
                                <div>
                                    <progress id="barra_bot"value={this.state.vidaporturno_bot} max={this.state.Monstruo[0].Vida}/>
                                    <div className={this.state.monstreBot} id="monstresPlayer2">

                                    <Row>
                                    <img src={this.state.monstreImgBot } id="imatge2" className="imatges" alt="imatgeMonstre"></img>
                                    </Row>
                                    
                                    {}   
                                </div>
                                
                                 
                           { !this.state.show == true ?
                        <div id="pantalla_registro2" >
                      
                            <Row>
                             <Col> Vida Jugador -  {this.state.vidaporturno_player}</Col>
                             <Col> Vida Bot -  {this.state.vidaporturno_bot}</Col>
                             </Row>
                             <Row>
                                 <div id="registro_texto2" >
                                 {!this.state.Registro ? "" : this.state.Registro.map((x, i) => {
                                        return (
                                       
                                               <div className="regtxt"> {x} </div>
                                         
                                        )
                                 })}
                                 </div>
                             </Row>
                       </div> : null
                       }
                                   
                                </div>
                                
                                
                                : null }
                                </div>
                                
                            } 
                             <div id="narrador2">
                           {!this.state.bloqueado == ""?  <b ID="bloqueado">Bloqueado </b>: null} 
                           {!this.state.esquivado == ""?  <b id="esquivado">Esquivado </b>: null} 
                           {!this.state.critico == ""?  <b id="critico">CRITICO!</b>: null} 
                           </div> 
                                
                                   
                </div>}
                
                {this.state.resultado == 0 ?  null:  <div id="fin_game"> {this.state.resultado == "win" ? <div ><button id="boton_perdiste" button onClick={() => this.ganabot()} >PERDISTE!</button></div> :<div>< button id="boton_ganaste" onClick={() => this.ganajugador()} >GANASTE!</button></div>}
                            </div>
                              }                     
            </div>
            </div>
            </div>
        );
    }
}