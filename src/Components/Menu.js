import React from 'react';
import { Link } from 'react-router-dom';
// import boton_perfil from '../imgs/Perfil/inventario/boton_perfil.png';
// import boton_pvp from '../imgs/Perfil/inventario/boton_pvp.png';
// import boton_ent from '../imgs/Perfil/inventario/boton_entrenamiento.png';
// import boton_amigos from '../imgs/Perfil/inventario/boton_amigos.png';
import '../Styles/Menu.css';
var boton_perfil ="https://bestiario.s-ul.eu/1PqFvCmj";
var boton_pvp ="https://bestiario.s-ul.eu/zjiAY85M";
var boton_ent="https://bestiario.s-ul.eu/Gh4HguRU";
var boton_amigos="https://bestiario.s-ul.eu/otlZW6GX";

export default class Menu extends React.Component {
    render() {
        return (
            <div id="submenu">
                {this.props.actiu ?
                    <div id="boton_menu">
                        <b className="boton">
                            <Link id="links" to="/Perfil"><img src={boton_perfil} alt="" /></Link>
                        </b>
                        <b className="boton">
                            <Link id="links" to="/Pvp"><img src={boton_pvp} alt="" /></Link>
                        </b>
                        <b className="boton">
                            <Link id="links" to="/Entrenamiento"><img src={boton_ent} alt="" /></Link>
                        </b>
                        <b className="boton">
                            <Link id="links" to="/Amics"><img src={boton_amigos} alt="" /></Link>
                        </b>
                    </div> :
                    <div id="boton_menu">
                        <b className="boton">
                            <img src={boton_perfil} alt="" />
                        </b>
                        <b className="boton">
                            <img src={boton_pvp} alt="" />
                        </b>
                        <b className="boton">
                            <img src={boton_ent} alt="" />
                        </b>
                        <b className="boton">
                            <img src={boton_amigos} alt="" />
                        </b>
                    </div>}
            </div>)
    }
}