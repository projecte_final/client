import React from 'react';
import Header from './Header';
import Menu from './Menu';
import '../Styles/Pvp.css';
import Axios from "axios";
import socket from './Socket';
import { useStopwatch } from 'react-timer-hook';

function Timer() {
    const {
        seconds,
        minutes
    } = useStopwatch({ autoStart: true })
    return (<div>
        <p>{minutes}:{seconds}</p>
    </div>);
}

export default class Pvp extends React.Component {
    _isMounted = false;
    constructor(props) {
        super(props)
        this.state = {
            imagenes: [],
            searchingBattle: false,
            monstreUser: "",
            monstreAdversari: "",
            batallaEncontrada: false,
            imagenMonstruoJugador: "",
            imagenMonstruoAdversario: "",
            ImagenesVisibles: true,
            empezarDiv: true,
            monstruoElegido: false,
            /* ANIMACIONES */
            monstrePlayer1: '',
            monstrePlayer2: '',
            registroBatallaVisible: false,
            vidaMonstreUsuari: 0,
            vidaMonstreAdversari: 0,
            vidaMonstreUsuariActual: 0,
            vidaMonstreAdversariActual: 0,
            guanyador: "",
            resultadoFinal: false,
            partidaTrobada: false,
            headerActiu: true
        };
        this.buscarBatalla = this.buscarBatalla.bind(this);
        this.seleccionarMonstruo = this.seleccionarMonstruo.bind(this);
        this.deseleccionarMonstruo = this.deseleccionarMonstruo.bind(this);
        this.ponerImagenesMonstruos = this.ponerImagenesMonstruos.bind(this);
        this.batallaTerminada = this.batallaTerminada.bind(this);
        this.cambiarAnimacion = this.cambiarAnimacion.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
        Axios.post("http://localhost:3001/api/GetImgMonstruos", { usuario: JSON.parse(sessionStorage.getItem("Usuari")).usuari })
            .then((res) => {
                if (this._isMounted) {
                    this.setState({ imagenes: res.data.rows })
                    var arrayEstadoSeleccionado = [];
                    for (var y = 0; y < this.state.imagenes.length; y++) {
                        arrayEstadoSeleccionado.push({ display: "none", triat: false })
                    }
                    this.setState({ monstruoSeleccionado: arrayEstadoSeleccionado });
                }
            })
        socket.on('batallaEncontrada', lucha => {
            //  console.log("registro", lucha.registro);
            if (this._isMounted) {
                if (lucha.usuariBatalla.find(datos => datos.sessionid === JSON.parse(sessionStorage.getItem("Usuari")).sessionid)) {
                    lucha.usuariBatalla.splice(lucha.usuariBatalla.findIndex(x => x.sessionid === JSON.parse(sessionStorage.getItem("Usuari")).sessionid), 1);
                    Axios.post('http://localhost:3001/api/GetMonstruo_info', { usuario: lucha.usuariBatalla[0].nombre, idmonstruo: lucha.usuariBatalla[0].idMonstre })
                        .then((res) => {
                            this.setState({
                                vidaMonstreAdversari: res.data.rows[0].Vida,
                                vidaMonstreAdversariActual: res.data.rows[0].Vida,
                                headerActiu: false
                            });

                            this.setState({
                                nomAdversari: lucha.usuariBatalla[0].nombre,
                                partidaTrobada: true,
                                monstreAdversari: lucha.usuariBatalla[0].idMonstre,
                                searchingBattle: false,
                                empezarDiv: false,
                                jugadors: [[1, lucha.registro[0][0].Nombre], [2, lucha.registro[0][1].Nombre]]
                            });
                            setTimeout(() => {
                                this.setState({ batallaEncontrada: true, partidaTrobada: false });
                                this.ponerImagenesMonstruos();
                                this.cambiarAnimacion(lucha.registro);
                            }, 3000)
                        })
                }
            }
        })
        // this.setState({monstruoSeleccionado: this.state.monstruoSeleccionado.push({seleccionado: "none"})})
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    buscarBatalla() {
        if (this.state.monstreUser !== "") {
            if (this.state.searchingBattle) {
                socket.emit('cancelarBusqueda', JSON.parse(sessionStorage.getItem("Usuari")).sessionid);
                var arrayTemporal = [...this.state.monstruoSeleccionado];
                arrayTemporal.forEach((x, indice) => {
                    arrayTemporal.splice(indice, 1, { display: "none", triat: false });
                });
                this.setState({ ImagenesVisibles: true, searchingBattle: false, monstreUser: "", monstruoSeleccionado: arrayTemporal, monstruoElegido: false, vidaMonstreUsuari: 0, vidaMonstreAdversari: 0 });
            }
            else {
                Axios.post('http://localhost:3001/api/GetMonstruo_info', { usuario: JSON.parse(sessionStorage.getItem("Usuari")).usuari, idmonstruo: this.state.monstreUser })
                    .then((res) => {
                        socket.emit('buscarBatalla', JSON.parse(sessionStorage.getItem("Usuari")).usuari, this.state.monstreUser, JSON.parse(sessionStorage.getItem("Usuari")).sessionid, res.data.rows[0])
                        this.setState({ vidaMonstreUsuari: res.data.rows[0].Vida, vidaMonstreUsuariActual: res.data.rows[0].Vida });
                    })

                this.setState({ searchingBattle: true, ImagenesVisibles: false });
            }
        }
    }

    seleccionarMonstruo(idMonstre, i) {
        var arrayTemporal = [...this.state.monstruoSeleccionado];
        arrayTemporal.forEach((x, indice) => {
            arrayTemporal.splice(indice, 1, { display: "none", triat: false });
        });
        arrayTemporal.splice(i, 1, { display: "block", triat: true });
        this.setState({ monstreUser: idMonstre, monstruoElegido: true, monstruoSeleccionado: arrayTemporal });
    }

    deseleccionarMonstruo(i) {
        var arrayTemporal = [...this.state.monstruoSeleccionado];
        arrayTemporal.forEach((x, indice) => {
            arrayTemporal.splice(indice, 1, { display: "none", triat: false });
        });
        this.setState({ monstreUser: "", monstruoElegido: false, monstruoSeleccionado: arrayTemporal });
    }

    ponerImagenesMonstruos() {
        Axios.get(`http://localhost:3001/api/getImatgeMonstre/${this.state.monstreUser}`).then((res) => this.setState({ imagenMonstruoJugador: res.data[0].img_gif }));
        Axios.get(`http://localhost:3001/api/getImatgeMonstre/${this.state.monstreAdversari}`).then((res) => this.setState({ imagenMonstruoAdversario: res.data[0].img_gif }));
    }

    async batallaTerminada() {
        var arrayTemporal = [...this.state.monstruoSeleccionado];
        arrayTemporal.forEach((x, indice) => {
            arrayTemporal.splice(indice, 1, { display: "none", triat: false });
        });


        await new Promise(resolve => setTimeout(resolve, 500));
        this.setState({
            searchingBattle: false,
            monstreUser: "",
            monstreAdversari: "",
            batallaEncontrada: false,
            imagenMonstruoJugador: "",
            imagenMonstruoAdversario: "",
            ImagenesVisibles: true,
            empezarDiv: true,
            monstruoElegido: false,
            /* ANIMACIONES */
            monstrePlayer1: '',
            monstrePlayer2: '',
            registroBatallaVisible: false,
            vidaMonstreUsuari: 0,
            vidaMonstreAdversari: 0,
            vidaMonstreUsuariActual: 0,
            vidaMonstreAdversariActual: 0,
            guanyador: "",
            resultadoFinal: false,
            partidaTrobada: false,
            nomAdversari: '',
            jugadors: '',
            monstruoSeleccionado: arrayTemporal,
            registroBatalla: '',
            headerActiu: true
        });
    }

    async cambiarAnimacion(arrayBatalla) {
        for (var vuelta = 0; vuelta <= arrayBatalla.length; vuelta++) {
            if (arrayBatalla[0][0].Nombre === JSON.parse(sessionStorage.getItem("Usuari")).usuari) {
                if (arrayBatalla[vuelta]) {
                    this.setState({ vidaMonstreUsuariActual: arrayBatalla[vuelta][0].Vida });
                    /* JUGADOR 1*/
                    if (arrayBatalla[vuelta][0].Vida > 0) {
                        await new Promise(resolve => setTimeout(resolve, 1000));
                        this.setState({
                            monstrePlayer1: 'monstrePlayer1'
                        })
                        await new Promise(resolve => setTimeout(resolve, 200));
                        this.setState({
                            monstrePlayer2: 'invertir-color',

                        })
                        await new Promise(resolve => setTimeout(resolve, 200));
                        this.setState({
                            monstrePlayer2: ''
                        })
                    }

                    await new Promise(resolve => setTimeout(resolve, 2000));
                    if (vuelta === 0) {
                        var arrayTemporal = [];
                        var arrayTemporalSecundaria = [];
                        arrayTemporalSecundaria.push(arrayBatalla[vuelta][0]);
                        arrayTemporal.push(arrayTemporalSecundaria);
                        this.setState({ registroBatalla: arrayTemporal, jugadorsNoms: arrayBatalla, registroBatallaVisible: true, });

                        var arrayTemporal2 = [...this.state.registroBatalla];
                        arrayTemporal2[vuelta].push(arrayBatalla[vuelta][1]);
                        this.setState({ registroBatalla: arrayTemporal2 });
                    } else if (arrayBatalla[vuelta][1]) {
                        var arrayTemporal = [...this.state.registroBatalla];
                        arrayTemporal[vuelta].push(arrayBatalla[vuelta][1]);
                        this.setState({ registroBatalla: arrayTemporal });
                    }

                    if (arrayBatalla[vuelta][1]) {
                        /* JUGADOR 2*/
                        this.setState({ vidaMonstreAdversariActual: arrayBatalla[vuelta][1].Vida });
                        if (arrayBatalla[vuelta][1].Vida > 0) {
                            await new Promise(resolve => setTimeout(resolve, 1000));
                            this.setState({
                                monstrePlayer2: 'monstrePlayer2'
                            })
                            await new Promise(resolve => setTimeout(resolve, 200));
                            this.setState({
                                monstrePlayer1: 'invertir-color',

                            })
                            await new Promise(resolve => setTimeout(resolve, 200));
                            this.setState({
                                monstrePlayer1: ''
                            })
                        }
                    }
                    if (arrayBatalla[vuelta + 1]) {
                        await new Promise(resolve => setTimeout(resolve, 2000));

                        var arrayTemporal = [...this.state.registroBatalla]
                        var arrayTemporalSecundaria = [];
                        arrayTemporalSecundaria.push(arrayBatalla[vuelta + 1][0]);
                        arrayTemporal.push(arrayTemporalSecundaria);
                        this.setState({ registroBatalla: arrayTemporal });
                    }
                }
            }
            else {
                if (arrayBatalla[vuelta]) {
                    /* JUGADOR 2*/
                    this.setState({ vidaMonstreAdversariActual: arrayBatalla[vuelta][0].Vida });
                    if (arrayBatalla[vuelta][0].Vida > 0) {
                        await new Promise(resolve => setTimeout(resolve, 1000));
                        this.setState({
                            monstrePlayer2: 'monstrePlayer2'
                        })
                        await new Promise(resolve => setTimeout(resolve, 200));
                        this.setState({
                            monstrePlayer1: 'invertir-color',

                        })
                        await new Promise(resolve => setTimeout(resolve, 200));
                        this.setState({
                            monstrePlayer1: ''
                        })

                    }

                    await new Promise(resolve => setTimeout(resolve, 2000));
                    if (vuelta === 0) {

                        var arrayTemporal = [];
                        var arrayTemporalSecundaria = [];
                        arrayTemporalSecundaria.push(arrayBatalla[vuelta][0]);
                        arrayTemporal.push(arrayTemporalSecundaria);
                        this.setState({ registroBatalla: arrayTemporal, jugadorsNoms: arrayBatalla, registroBatallaVisible: true, });

                        var arrayTemporal2 = [...this.state.registroBatalla];
                        arrayTemporal2[vuelta].push(arrayBatalla[vuelta][1]);
                        this.setState({ registroBatalla: arrayTemporal2 });


                    } else if (arrayBatalla[vuelta][1]) {
                        var arrayTemporal = [...this.state.registroBatalla];
                        arrayTemporal[vuelta].push(arrayBatalla[vuelta][1]);
                        this.setState({ registroBatalla: arrayTemporal });
                    }

                    if (arrayBatalla[vuelta][1]) {
                        /* JUGADOR 1*/
                        this.setState({ vidaMonstreUsuariActual: arrayBatalla[vuelta][1].Vida });
                        if (arrayBatalla[vuelta][1].Vida > 0) {
                            await new Promise(resolve => setTimeout(resolve, 1000));
                            this.setState({
                                monstrePlayer1: 'monstrePlayer1'
                            })
                            await new Promise(resolve => setTimeout(resolve, 200));
                            this.setState({
                                monstrePlayer2: 'invertir-color',

                            })
                            await new Promise(resolve => setTimeout(resolve, 200));
                            this.setState({
                                monstrePlayer2: ''
                            })
                        }
                    }
                    if (arrayBatalla[vuelta + 1]) {
                        await new Promise(resolve => setTimeout(resolve, 2000));
                        var arrayTemporal = [...this.state.registroBatalla]
                        var arrayTemporalSecundaria = [];
                        arrayTemporalSecundaria.push(arrayBatalla[vuelta + 1][0]);
                        arrayTemporal.push(arrayTemporalSecundaria);
                        this.setState({ registroBatalla: arrayTemporal });
                    }

                }
            }
        }
        /* GANADOR */
        if (this.state.vidaMonstreUsuariActual > 0) {
            this.setState({ guanyador: JSON.parse(sessionStorage.getItem("Usuari")).usuari, resultadoFinal: true });
        } else {
            this.setState({ guanyador: this.state.nomAdversari, resultadoFinal: true });
        }
        this.setState({ headerActiu: true });
    }

    render() {
        return (
            <div id="todo">
                <div className="clouds"></div>
                <div className="fogContainer">
                    <div className="fog">
                    </div>
                    <div id="containerlog" className="perfil" >
                        <Header userName={JSON.parse(sessionStorage.getItem("Usuari")).usuari} actiu={this.state.headerActiu} />
                        <Menu actiu={this.state.headerActiu} />
                        <div className="Pvpcontainer">
                            {this.state.partidaTrobada ?
                                <div className="BatallaEncontrada">
                                    <p>Combat trobat contra: {this.state.nomAdversari}</p>
                                </div> : null}
                            {this.state.resultadoFinal ?
                                <div className="ResultadoFinal BatallaEncontrada" onClick={this.batallaTerminada}>
                                    <p>Ha guanyat {this.state.guanyador}</p>
                                </div> : null}
                            {this.state.empezarDiv ?
                                <div className="BuscandoBatalla" onClick={() => this.buscarBatalla()}>
                                    {this.state.searchingBattle ? <div className="buscantDiv"><h1>BUSCANT OPONENT</h1><Timer /><p>CANCELAR COMBAT</p></div> :
                                        <div>{!this.state.monstruoElegido ? <h1>SELECCIONA UN MONSTRE</h1> : <h1>BUSCAR COMBAT</h1>}</div>
                                    }
                                </div> : null}
                            {this.state.batallaEncontrada ?
                                <div className="Juego">
                                    <progress value={this.state.vidaMonstreUsuariActual} max={this.state.vidaMonstreUsuari} className="ProgressBarJugador" />
                                    <div className="AreaCombate">
                                        <img src={this.state.imagenMonstruoJugador} className={this.state.monstrePlayer1} alt="MonstruoJugador"></img>
                                        <img src={this.state.imagenMonstruoAdversario} className={this.state.monstrePlayer2} alt="MonstruoAdversario"></img>
                                    </div>
                                    <progress value={this.state.vidaMonstreAdversariActual} max={this.state.vidaMonstreAdversari} className="ProgressBarAdversario" />
                                </div>
                                : null}
                        </div>
                        {this.state.ImagenesVisibles && this.state.monstruoSeleccionado ? <div className="TriaMontruo">

                            {this.state.imagenes.map((x, i) => {
                                return (
                                    <div className="mon" key={i}>
                                        <div className="monstruoSeleccionado" style={{ display: this.state.monstruoSeleccionado[i].display }}></div>
                                        <div className="hoverMonstruo">
                                            {!this.state.monstruoSeleccionado[i].triat ?
                                                <div className="Triar alignTextTriar" onClick={() => this.seleccionarMonstruo(x.idMonstre, i)}>
                                                    <p>Triar</p>
                                                </div> :
                                                <div className="Triat alignTextTriar" onClick={() => this.deseleccionarMonstruo(i)}>
                                                    <p>Triat</p>
                                                </div>}
                                            <div className="VeureEstadistiques alignTextTriar">
                                                <p>Estadistiques</p>
                                            </div>
                                        </div>
                                        <img src={x.img} className="imagenesTria" alt="imatgeMonstre"></img>
                                        <p>{x.Nom}</p>
                                    </div>
                                )
                            })}
                        </div> : null}
                        {this.state.registroBatallaVisible ? <div className="background_Formularis RegistroPartida">
                            {this.state.registroBatalla.map((x, i) => {
                                return (
                                    <div key={i}>
                                        {x.map((o, indice) => {
                                            if (i === 0 && indice === 0) {
                                                return (
                                                    <div key={indice}>
                                                        <p>| Jugador 1  {this.state.jugadorsNoms[0][0].Nombre} || Jugador 2  {this.state.jugadorsNoms[0][1].Nombre}</p>
                                                        <p>| Jugador 1 | Vida: {x[indice].Vida}</p>
                                                    </div>
                                                )
                                            }
                                            else if ((indice % 2) === 1) {
                                                if (x[indice].Esquiva === -1) {
                                                    return (
                                                        <div key={indice}>
                                                            <p>| Jugador {this.state.jugadors[indice][0]} | Vida Restant: {x[indice].Vida} | Atac Enemic: {x[indice - 1].Dany} | Defensa: {x[indice].Armadura} </p>
                                                        </div>
                                                    )
                                                }
                                                else {
                                                    return (
                                                        <div key={indice}>
                                                            <p>| Jugador {this.state.jugadors[indice][0]} | Vida Restant: {x[indice].Vida} | Atac Enemic: {x[indice - 1].Dany} | Esquiva: {x[indice].Esquiva} </p>
                                                        </div>
                                                    )
                                                }
                                            }
                                            else {
                                                if (x[indice].Esquiva === -1) {
                                                    return (
                                                        <div key={indice}>
                                                            <p>| Jugador {this.state.jugadors[indice][0]} | Vida Restant {x[indice].Vida} | Atac Enemic: {this.state.registroBatalla[i - 1][indice + 1].Dany} | Defensa: {x[indice].Armadura}</p>
                                                        </div>
                                                    )

                                                }
                                                else {
                                                    return (
                                                        <div key={indice}>
                                                            <p>| Jugador {this.state.jugadors[indice][0]} | Vida Restant: {x[indice].Vida} | Atac Enemic: {this.state.registroBatalla[i - 1][indice + 1].Dany} | Esquiva: {x[indice].Esquiva}</p>
                                                            <p></p>
                                                        </div>
                                                    )
                                                }
                                            }
                                        })}
                                    </div>
                                )
                            })}
                        </div> : null}
                    </div>
                </div>
            </div>
        )
    }
}