import React from "react";
import { Container, Row, Col } from 'react-grid-system';
import Button from 'react-bootstrap/Button';
import '../Styles/administracion.css';
import Axios from "axios";
import Menu from './Menu';
import Header from './Header';
import { ProgressBar } from 'react-bootstrap';
// import boton from '../imgs/Login/boton.png';
import { Link } from 'react-router-dom';
var boton = "https://bestiario.s-ul.eu/eJrajNQF";
// import 'bootstrap/dist/css/bootstrap.min.css';
export default class administracion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            idmonstre:0,
            monstruonuevonombre:"",
            imgmonstre:"",
            gifmonstruo:"",
            nivellmonstre: -1,
            vidamonstre: -1,
            danymonstre: -1,
            armaduramonstre: -1,
            esquivamonstre: -1,
            descripciomonstre: "",
            usuarinou: "",
            contrasenya: "",
            Correu:"",
            usuarioaeliminar:""
        };
        this.componentDidMount = this.componentDidMount.bind(this)  
  
    }
    async componentDidMount() {
    
    }
    cambiaridmonstre=(e)=>{
        console.log(e.target.value);
        this.setState({idmonstre: e.target.value})
        console.log(this.state.idmonstre);
      }
    nomnewmonstrenombre=(e)=>{
        console.log(e.target.value);
        this.setState({monstruonuevonombre: e.target.value})
        console.log(this.state.idmonstre);
      }
    imgnuevomonstruo=(e)=>{
        console.log(e.target.value);
        this.setState({imgmonstre: e.target.value})
        console.log(this.state.idmonstre);
      }
      gifnuevomonstruo=(e)=>{
        console.log(e.target.value);
        this.setState({gifmonstruo: e.target.value})
        console.log(this.state.idmonstre);
      } 
      setnivell=(e)=>{
        console.log(e.target.value);
        this.setState({nivellmonstre: e.target.value})
        console.log(this.state.idmonstre);
      }
      setVida=(e)=>{
        console.log(e.target.value);
        this.setState({vidamonstre: e.target.value})
        console.log(this.state.idmonstre);
      }
      setDany=(e)=>{
        console.log(e.target.value);
        this.setState({danymonstre: e.target.value})
        console.log(this.state.idmonstre);
      }
      setArmadura=(e)=>{
        console.log(e.target.value);
        this.setState({armaduramonstre: e.target.value})
        console.log(this.state.idmonstre);
      }
      setEsquiva=(e)=>{
        console.log(e.target.value);
        this.setState({esquivamonstre: e.target.value})
        console.log(this.state.idmonstre);
      }
      setDescripcio=(e)=>{
        console.log(e.target.value);
        this.setState({descripciomonstre: e.target.value})
        console.log(this.state.idmonstre);
      }
      eliminarmonstre(){
          console.log(this.state.idmonstre);
        if(this.state.idmonstre != null){
            Axios.post("http://localhost:3001/api/admin_eliminarmonstre", { idmonstruo: this.state.idmonstre });
        }
      }
      crearmonstre(){
        if(this.state.monstruonuevonombre != "" && this.state.imgmonstre != "" && this.state.gifmonstruo != "" && this.state.descripciomonstre != "" && this.state.nivellmonstre >= 0 && this.state.vidamonstre >= 0 && this.state.danymonstre >= 0 && this.state.armaduramonstre >= 0 && this.state.esquivamonstre >= 0){
        console.log("Vida: "+this.state.vidamonstre+" Nom: "+this.state.monstruonuevonombre+" Descripcio: "+this.state.descripciomonstre+" nivel: "+this.state.nivellmonstre)
          Axios.post("http://localhost:3001/api/crear_monstre",
           {
            nommonstre: this.state.monstruonuevonombre,
            imgmonstre: this.state.imgmonstre,
            gifmonstre: this.state.gifmonstruo,
            descripciomonstre: this.state.descripciomonstre,
            nivellmonstre: this.state.nivellmonstre,
            vidamonstre: this.state.vidamonstre,
            danymonstre: this.state.danymonstre,
            armaduramonstre: this.state.armaduramonstre,
            esquivamonstre: this.state.esquivamonstre
        
        });
        }else{
            alert("completa tots el camps");
        }
      
    }
    setUsuarinou=(e)=>{
        console.log(e.target.value);
        this.setState({usuarinou: e.target.value})
 
      }
      setContrasenyanova=(e)=>{
        console.log(e.target.value);
        this.setState({contrasenya: e.target.value})
        
      }
      setCorreu=(e)=>{
        console.log(e.target.value);
        this.setState({Correu: e.target.value})
        
      }
      setusuarioaeliminar=(e)=>{
        console.log(e.target.value);
        this.setState({usuarioaeliminar: e.target.value})
        
      }
      
      crearusuari(){
    
        if(this.state.usuarinou != "" && this.state.contrasenya !="" && this.state.Correu != ""){
            Axios.post("http://localhost:3001/api/crearusuari", { usuario: this.state.usuarinou, contrasenya: this.state.contrasenya, correu: this.state.Correu });
        }
      }
      eliminarusaurio(){
            if(this.state.usuarioaeliminar != ""){
                Axios.post("http://localhost:3001/api/eliminarusuario", { usuario: this.state.usuarioaeliminar});
            }
      }
     
    render() {
        return (
            <div id="todo">
             
             <div className="clouds"></div>
                <div className="fogContainer">
                    <div className="fog">  
                    </div>
                    <div id="panel_administracion" >
                    <div>
                            <Row><h2 id="color">Inserir Monstre Nou</h2></Row>
                            <Row id="formulario1" className="margin">
                                <Col><b id="color">Nom del Monstre</b></Col>
                                <Col><input type="text" value={this.state.value} onChange={this.nomnewmonstrenombre} /></Col>
                            </Row>
                            <Row id="formulario1" className="margin">
                                <Col><b id="color">Imatge(link)</b></Col>
                                <Col><input type="text" value={this.state.value} onChange={this.imgnuevomonstruo} /></Col>
                            </Row>
                            <Row id="formulario1" className="margin">
                                <Col><b id="color">Imatge Gif (link)</b></Col>
                                <Col><input type="text" value={this.state.value} onChange={this.gifnuevomonstruo} /></Col>
                            </Row>
                            <Row id="formulario1" className="margin">
                                <Col><b id="color">Nivell</b></Col>
                                <Col><input type="number" value={this.state.value} onChange={this.setnivell} /></Col>
                       
                            </Row>
                            <Row id="formulario1" className="margin margin2">
                                <Col><b id="color">Vida</b></Col>
                                <Col><input type="number" value={this.state.value} onChange={this.setVida} /></Col>
                                <Col><b id="color">Dany</b></Col>
                                <Col><input type="number" value={this.state.value} onChange={this.setDany} /></Col>
                            </Row>
                            <Row id="formulario1" className="margin margin2">
                                <Col><b id="color">Armadura</b></Col>
                                <Col><input type="number" value={this.state.value} onChange={this.setArmadura}/> </Col>
                                <Col><b id="color">Esquiva</b></Col>
                                <Col><input type="number" value={this.state.value} onChange={this.setEsquiva}/></Col>
                            </Row>                                
                            <Row id="formulario1" className="margin">
                                <Col><b id="color">Descripció</b></Col>
                                <Col>
                                    <textarea id="story" name="story"rows="3" cols="30" value={this.state.value} onChange={this.setDescripcio}/>
                                </Col>
                            </Row>
                            <button onClick={() => this.crearmonstre()} type="submit" className="colormargin">Crear</button>
                            </div>
                            <div>
                            <Row><h2 id="color">Eliminar Monstre</h2></Row>
                            <Row id="formulario1" className="margin">
                                <Col><b id="color">ID Monstre</b></Col>
                                <Col><input type="number" value={this.state.value} onChange={this.cambiaridmonstre} /></Col>
                            </Row>
                            <button onClick={() => this.eliminarmonstre()} type="submit" className="colormargin">Elimnar</button>
                            </div>
                            <div>
                            <Row><h2 id="color">Crea Usuari</h2></Row>
                            <Row id="formulario1" className="margin">
                                <Col><b id="color">Nom Usuari</b></Col>
                                <Col><input type="text" value={this.state.value} onChange={this.setUsuarinou} /></Col>
                            </Row>
                            <Row id="formulario1" className="margin">
                                <Col><b id="color">Contrasenya</b></Col>
                                <Col><input type="text" value={this.state.value} onChange={this.setContrasenyanova} /></Col>
                            </Row>
                            <Row id="formulario1" className="margin">
                                <Col><b id="color">Correu</b></Col>
                                <Col><input type="text" value={this.state.value} onChange={this.setCorreu} /></Col>
                            </Row>
        
                            <button onClick={() => this.crearusuari()} type="submit" className="colormargin">Crear Usuari</button>
                            </div>
                        <div>
                            <Row><h2 id="color">Eliminar Usuari</h2></Row>
                            <Row id="formulario1" className="margin">
                                <Col><b id="color">Nom Usuari</b></Col>
                                <Col><input type="text" value={this.state.value} onChange={this.setusuarioaeliminar} /></Col>
                            </Row>
                            <button onClick={() => this.eliminarusaurio()} type="submit" className="colormargin">Eliminar usuario</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}